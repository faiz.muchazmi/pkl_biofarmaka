=== Vihaan Blog Lite === 
Contributors: wponlinesupport, anoopranawat
Requires at least: WordPress 4.5
Tested up to: WordPress 4.9.7
Version:  1.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, blog,  left-sidebar,  right-sidebar,  custom-background,  custom-colors,  custom-menu,  featured-images,  full-width-template,  threaded-comments,  rtl-language-support,  footer-widgets,  sticky-post,  theme-options

== Description ==

Vihaan Blog Lite is a beautiful WordPress blog theme for blogs website. Theme is lightweight, fast and optimized for all mobile phones. Theme comes with latest post slider, post list and grid view.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Upload vihaan-blog-lite.zip file.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel.


== Copyright ==

Vihaan Blog Lite WordPress Theme, Copyright 2018 WP Online Support
Vihaan Blog Lite is Distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Bundled Licenses ==

Font Icons
License: - Font: SIL OFL 1.1, Icons: CC BY 4.0 License, Code: MIT License (http://fontawesome.io/license)
Source : http://fontawesome.io

Owl Carousel v2.3.4
Copyright 2013-2018 David Deutsch
Licensed under:  MIT License 
Source : https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE

TGM Plugin Activation 2.6.1
License: GPL-2.0+
Copyright (c) 2011, Thomas Griffin
https://opensource.org/licenses/GPL-2.0

Images for screenshot
License: Creative Commons Zero (CC0) license (https://www.pexels.com/photo-license/)
- https://www.pexels.com/
- https://www.pexels.com/photo/woman-wearing-red-purple-and-pink-floral-v-neck-cap-sleeved-dress-drinking-through-clear-wine-glass-936017/
- https://www.pexels.com/photo/photography-of-woman-sitting-on-chair-near-window-761872/
- https://www.pexels.com/photo/woman-holding-her-child-walking-near-windmills-122101/



== Changelog ==

== 1.0.2 ==
* Fixed design issue if theme using any pluign with Owl Carousel.
* Removed unwanted functions.

== 1.0.1 ==
* Fixed multiple category issue under latest post slider section

= 1.0 =
* Initial release
