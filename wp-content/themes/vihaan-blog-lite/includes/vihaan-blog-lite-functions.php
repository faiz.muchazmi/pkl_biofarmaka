<?php
/**
 * Functions File
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Update theme default settings
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_default_settings() {

	$default_settings = array(		
									
                                    'menu_bar_bg_clr'                   => '#fff',
                                    'menu_bar_link_clr'                 => '#000',
                                    'menu_bar_linkh_clr'                => '#08f',
                                    'continue_read_clr'                 => '#000',
                                    'continue_read_hvr_clr'             => '#08f',
                                    'h1_clr'                       		=> '#000000',
                                    'h2_clr'                       		=> '#000000',
                                    'h3_clr'                       		=> '#000000',
                                    'h4_clr'                       		=> '#000000',
                                    'h5_clr'                       		=> '#000000',
                                    'h6_clr'                       		=> '#000000',
                                    'link_clr'                          => '#000000',
                                    'hover_link_clr'                    => '#08f',

									'dispable_slider'                   => 0,
									'number_of_lastestpost'             => 3,
									'select_category_for_slider'        => 1,
									
                                    'blog_excerpt_length'               => 40,
                                    'blog_show_date'                    => 1,
                                    'blog_show_author'                  => 1,
                                    'blog_show_cat'                     => 1,
                                    'blog_show_tags'                    => 1,
									'blog_show_comment'                 => 1,
									'blog_show_readmore'                => 1,
									                                  
                                    'cat_show_date'                     => 1,
                                    'cat_show_author'                   => 1,
                                    'cat_show_cat'                      => 1,                                 
                                    'cat_show_tags'                     => 1,                                   
									'cat_show_comment'                  => 1,
									'cat_show_readmore'                 => 1,                                  
                                    'single_post_fet_img'               => 1,
                                    'related_post_show'                 => 0,
                                    'related_post_title'                => __('More from', 'vihaan-blog-lite') ,                                                                                     
                                    
                                    'footer_social'                     => 0,
                                    'facebook'                          => '',
                                    'twitter'                           => '',
                                    'linkedin'                          => '',
                                    'behance'                           => '',
                                    'dribbble'                          => '',
                                    'instagram'                         => '',
                                    'youtube'                           => '',
                                    'pinterest'                         => '',
                                    'copyright'                         => __('Blog Theme', 'vihaan-blog-lite'),
                                   
                            );

	return apply_filters('vihaan_blog_lite_options_default_values', $default_settings );
}

/**
 * Escape Tags & Slashes
 *
 * Handles escapping the slashes and tags
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_esc_attr($data) {
    return esc_attr( $data );
}


/**
 * Function to excerpt length
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_excerpt_length( $length ) {
	$blog_excerpt_length 	= vihaan_blog_lite_get_theme_mod( 'blog_excerpt_length' );
	if ( ! is_admin() ) {
		return $blog_excerpt_length;
	}	
}
add_filter( 'excerpt_length', 'vihaan_blog_lite_excerpt_length', 999 );

/**
 * Function to excerpt more
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_excerpt_more( $more ) {
	if ( ! is_admin() ) {	
		return '&hellip;';
	}	
}
add_filter('excerpt_more', 'vihaan_blog_lite_excerpt_more');


/**
 * Function to get footer sidebar widget class
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_footer_widgets_cls( $sidebar_id ) {	
	global $_wp_sidebars_widgets;	
	
	$sidebars_widgets_count = $_wp_sidebars_widgets;

	if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) {

		$widget_count 	= count( $sidebars_widgets_count[ $sidebar_id ] );
		$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );

		if ( $widget_count == 2 ) {			
			$widget_classes .= ' vihaan-blog-lite-col-6';
		} elseif  ( $widget_count == 3 ) {			
			$widget_classes .= ' vihaan-blog-lite-col-4';
		} elseif ( $widget_count == 4 ) {			
			$widget_classes .= ' vihaan-blog-lite-col-3';
		}  elseif ( $widget_count == 5 ) {			
			$widget_classes .= ' vihaan-blog-lite-col-5c';
		}  elseif ( $widget_count == 6 ) {			
			$widget_classes .= ' vihaan-blog-lite-col-2';
		} else {			
			$widget_classes .= ' vihaan-blog-lite-col-12';
		}
		return $widget_classes;
	}
}


/**
 * Function to get customizer value
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_get_theme_mod( $mod = '' ) {
	
	$default_settings 	= vihaan_blog_lite_default_settings();
	$default_val 		= isset($default_settings[ $mod ]) ? $default_settings[ $mod ] : '';
    
        return get_theme_mod( $mod, $default_val );
}

/**
 * Clean variables using sanitize_text_field. Arrays are cleaned recursively.
 * Non-scalar values are ignored.
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_sanitize_clean( $var ) {
	if ( is_array( $var ) ) {
		return array_map( 'vihaan_blog_lite_sanitize_clean', $var );
	} else {
		$data = is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
		return wp_unslash($data);
	}
}

/**
 * Checkbox sanitization callback.
 */
function vihaan_blog_lite_sanitize_checkbox( $checked ) {
	return ( ( !empty( $checked ) ) ? true : false );
}

/**
 * Select sanitization callback.
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_sanitize_select( $input, $setting ) {
	$input = sanitize_key( $input );
	$choices = $setting->manager->get_control( $setting->id )->choices;
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Sanitize URL
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_sanitize_url( $url ) {
	return esc_url_raw( trim($url) );
}

/**
 * Handles the footer copy right text
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_footer_copyright() {
	
	$current_year 	= date( 'Y', current_time('timestamp') );
	$copyright_text = vihaan_blog_lite_get_theme_mod( 'copyright' );
	$copyright_text = str_replace('{year}', $current_year, $copyright_text);

	return apply_filters( 'vihaan_blog_lite_footer_copyright', $copyright_text);

}

/**
 * Social icon Footer
 *
 * @package vihaan_blog_lite
 * @since 1.0
 */

function vihaan_blog_lite_get_footer_social_icon() {

		$facebook 		= vihaan_blog_lite_get_theme_mod( 'facebook' ); 
		$twitter 		= vihaan_blog_lite_get_theme_mod( 'twitter' );
		$linkedin 		= vihaan_blog_lite_get_theme_mod( 'linkedin' );
		$behance 		= vihaan_blog_lite_get_theme_mod( 'behance' );   
		$dribbble 		= vihaan_blog_lite_get_theme_mod( 'dribbble' );
		$instagram 		= vihaan_blog_lite_get_theme_mod( 'instagram' );
		$youtube 		= vihaan_blog_lite_get_theme_mod( 'youtube' );
		$pinterest      = vihaan_blog_lite_get_theme_mod( 'pinterest' );
		

     	if(!empty($facebook) ) { ?>	
       		<a href="<?php echo esc_url($facebook); ?>" title="<?php esc_attr_e('Facebook','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-facebook-icon"><i class="fa fa-facebook"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Facebook', 'vihaan-blog-lite' ); ?></span></a>
		<?php } 
		if(!empty($twitter) ) { ?>	
			<a href="<?php echo esc_url($twitter); ?>" title="<?php esc_attr_e('Twitter','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-twitter-icon"><i class="fa fa-twitter"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Twitter', 'vihaan-blog-lite' ); ?></span></a>
		<?php } 
		if(!empty($linkedin) ) { ?>	
			<a href="<?php echo esc_url($linkedin); ?>" title="<?php esc_attr_e('LinkedIn','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-linkedin-icon"><i class="fa fa-linkedin"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'LinkedIn', 'vihaan-blog-lite' ); ?></span></a>
		<?php } 
		if(!empty($youtube)) { ?>		
			<a href="<?php echo esc_url($youtube); ?>" title="<?php esc_attr_e('YouTube','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-youtube-icon"><i class="fa fa-youtube"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'YouTube', 'vihaan-blog-lite' ); ?></span></a>				
		<?php } 
		if(!empty($instagram) ) { ?>		
			<a href="<?php echo esc_url($instagram); ?>" title="<?php esc_attr_e('Instagram','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-instagram-icon"><i class="fa fa-instagram"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Instagram', 'vihaan-blog-lite' ); ?></span></a>				
		<?php } 
		if(!empty($behance) ) { ?>		
			<a href="<?php echo esc_url($behance); ?>" title="<?php esc_attr_e('Behance','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-behance-icon"><i class="fa fa-behance"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Behance', 'vihaan-blog-lite' ); ?></span></a>				
		<?php } 

		if(!empty($dribbble) ) { ?>		
			<a href="<?php echo esc_url($dribbble); ?>" title="<?php esc_attr_e('Dribbble','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-dribbble-icon"><i class="fa fa-dribbble"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Dribbble', 'vihaan-blog-lite' ); ?></span></a>				
		<?php } 

		if(!empty($pinterest) ) { ?>		
			<a href="<?php echo esc_url($pinterest); ?>" title="<?php esc_attr_e('Pinterest','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-pinterest-icon"><i class="fa fa-pinterest"></i> <span class="wpiclt-social-text"><?php esc_html_e( 'Pinterest', 'vihaan-blog-lite' ); ?></span></a>				
		<?php }

}

/**
 * Mobile Social icon header
 *
 * @package vihaan_blog_lite
 * @since 1.0
 */
function vihaan_blog_lite_get_header_social_icon() {

		$facebook 		= vihaan_blog_lite_get_theme_mod( 'facebook' ); 
		$twitter 		= vihaan_blog_lite_get_theme_mod( 'twitter' );
		$linkedin 		= vihaan_blog_lite_get_theme_mod( 'linkedin' );
		$behance 		= vihaan_blog_lite_get_theme_mod( 'behance' );   
		$dribbble 		= vihaan_blog_lite_get_theme_mod( 'dribbble' );
		$instagram 		= vihaan_blog_lite_get_theme_mod( 'instagram' );
		$youtube 		= vihaan_blog_lite_get_theme_mod( 'youtube' );
		$pinterest      = vihaan_blog_lite_get_theme_mod( 'pinterest' );
	

     	if(!empty($facebook) ) { ?>   
	            <a href="<?php echo esc_url($facebook); ?>" title="<?php esc_attr_e('Facebook','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-facebook-icon"><i class="fa fa-facebook"></i></a>
	    <?php } 
	    if(!empty($twitter) ) { ?>  
	            <a href="<?php echo esc_url($twitter); ?>" title="<?php esc_attr_e('Twitter','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-twitter-icon"><i class="fa fa-twitter"></i></a>
	    <?php } 
	    if(!empty($linkedin) ) { ?> 
	            <a href="<?php echo esc_url($linkedin); ?>" title="<?php esc_attr_e('LinkedIn','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-linkedin-icon"><i class="fa fa-linkedin"></i></a>
	    <?php } 
	    if(!empty($youtube)) { ?>       
	            <a href="<?php echo esc_url($youtube); ?>" title="<?php esc_attr_e('YouTube','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-youtube-icon"><i class="fa fa-youtube"></i></a>             
	    <?php } 
	    if(!empty($instagram) ) { ?>        
	            <a href="<?php echo esc_url($instagram); ?>" title="<?php esc_attr_e('Instagram','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-instagram-icon"><i class="fa fa-instagram"></i></a>             
	    <?php } 
	    if(!empty($behance) ) { ?>      
	            <a href="<?php echo esc_url($behance); ?>" title="<?php esc_attr_e('Behance','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-behance-icon"><i class="fa fa-behance"></i></a>             
	    <?php } 
	    if(!empty($dribbble) ) { ?>     
	            <a href="<?php echo esc_url($dribbble); ?>" title="<?php esc_attr_e('Dribbble','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-dribbble-icon"><i class="fa fa-dribbble"></i></a>             
	    <?php }
	    if(!empty($pinterest) ) { ?>      
                <a href="<?php echo esc_url($pinterest); ?>" title="<?php esc_attr_e('Pinterest','vihaan-blog-lite'); ?>" target="_blank" class="wpiclt-sn-icon wpiclt-pinterest-icon"><i class="fa fa-pinterest"></i></a>             
	    <?php }

}