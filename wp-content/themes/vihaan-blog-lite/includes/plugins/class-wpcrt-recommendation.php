<?php
/**
 * vihaan_blog_lite_Recommendation Class
 * 
 * Handles the recommended plugin functionality.
 * 
 * @package Vihaan Blog Lite
 * @since 1.0
 */

// Plugin recomendation class
require_once( VIHAAN_BLOG_LITE_DIR . '/includes/plugins/class-tgm-plugin-activation.php' );

class vihaan_blog_lite_Recommendation {

	function __construct() {
		
		// Action to add recomended plugins
		add_action( 'tgmpa_register', array($this, 'vihaan_blog_lite_recommend_plugin') );
	}

	/**
	 * Recommend Plugins
	 * 
	 * @package Vihaan Blog Lite
	 * @since 1.0
	 */
	function vihaan_blog_lite_recommend_plugin() {
	    $plugins = array(	       
	        array(
	            'name'               => __('Instagram Slider and Carousel Plus Widget', 'vihaan-blog-lite'),
	            'slug'               => 'slider-and-carousel-plus-widget-for-instagram',
	            'required'           => false,
	        ),
	        array(
	            'name'               => __('Featured Post Creative', 'vihaan-blog-lite'),
	            'slug'               => 'featured-post-creative',
	            'required'           => false,
	        )
	    );
	    tgmpa( $plugins);
	}
}

$vihaan_blog_lite_recommendation = new vihaan_blog_lite_Recommendation();