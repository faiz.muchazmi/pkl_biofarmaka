<?php
/**
 * 
 * Template part for including posts design formate wise
 * @link https://codex.wordpress.org/Template_Hierarchy
 * 
 * @package Vihaan Blog Lite
 * @since 1.0 
 * 
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
	<div class="vihaan-blog-lite-post-innr clearfix <?php if ( !has_post_thumbnail() ) { echo 'no-thumb-image'; } ?>">   
		
		<div class="vihaan-blog-lite-col-6 vihaan-blog-lite-columns">  
					<?php get_template_part( 'template-parts/content', 'media' ); ?>
		</div>	
		<div class="blogdesign-post-grid-content <?php if ( !has_post_thumbnail() ) { echo 'vihaan-blog-lite-col-12'; } else { echo 'vihaan-blog-lite-col-6';} ?>  vihaan-blog-lite-columns">				
				<header class="entry-header">
					
						<?php if (is_sticky()) : ?>
										<span class="sticky-label"><i class="fa fa-thumb-tack"></i><span class="sticky-label-text"><?php esc_html_e('Featured', 'vihaan-blog-lite'); ?></span></span>
						<?php endif; ?>
					    <?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_cat_posted_on(); }   ?>
						<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
								
						<?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_posted_on(); }  ?>
								
				</header><!-- .entry-header -->
				<?php
				if ( is_search() ) { ?>
						<div class="entry-summary">
				<?php    
						the_excerpt();
				} else { ?>
						<div class="entry-content">
				<?php             
						$ismore = ! empty( $post->post_content ) ? strpos( $post->post_content, '<!--more-->' ) : '';
						if ( ! empty( $ismore ) ) {
								/* translators: %s: Name of current post */
								the_content( sprintf(
												esc_html__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'vihaan-blog-lite' ),
												get_the_title()
										) );
						} else {
								the_excerpt();				
						}
						wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vihaan-blog-lite' ),
								'after'  => '</div>',
						) );

				}
				 ?>
				 <?php vihaan_blog_lite_tags_posted_on(); ?>
				 
					<footer class="entry-footer">
							<?php vihaan_blog_lite_entry_footer(); ?>
					</footer><!-- .entry-footer -->
					
					</div><!-- .entry-content --><!-- .entry-summary -->
				</div>       
	</div>
</article><!-- #post-## -->