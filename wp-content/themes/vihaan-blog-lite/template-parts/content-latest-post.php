<?php	
$number_of_lastestpost = get_theme_mod( 'number_of_lastestpost' );
$cat = get_theme_mod( 'select_category_for_slider' );
	
			
	// WP Query Parameters
	$args = array ( 
		'post_type'      		=> 'post',
		'post_status' 			=> array('publish'),
		'order'          		=> 'DESC',
		'cat'                   => esc_html($cat),
		'orderby'        		=> 'date', 
		'posts_per_page' 		=> absint($number_of_lastestpost),
		'ignore_sticky_posts'	=> true,
		
	);

	// WP Query
	$query 		= new WP_Query($args);		

	// If post is there
	if( $query->have_posts() ) {
		?>
		
			<div class="owl-carousel owl-carousel-post-slider">
				<?php
					while ($query->have_posts()) : $query->the_post();
					$featured_img_url = get_the_post_thumbnail_url($post->ID,'large');
					
				?>
					<div class="owl-carousel-post-slide">						
							<div class="entry-media">
								<a class="entry-image-link" href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo esc_url($featured_img_url); ?>)"></a>
							
								<div class="vihaan-blog-lite-content-overlay">	
									<?php vihaan_blog_lite_cat_posted_on(); ?>
									<div class="entry-title-header bgs">							
										<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									</div>									
									 <?php vihaan_blog_lite_latest_posted_on(); ?> 
									
									<div class="entry-summary">
										<?php  the_excerpt(); ?>
									</div>	
								</div>
							</div>							
					</div>
				<?php endwhile;  ?>
			</div><!-- end .row -->

<?php
	wp_reset_postdata();// Reset query
} // End of check have post