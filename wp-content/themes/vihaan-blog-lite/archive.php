<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */


get_header(); ?>
<div class="content-row">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			?>		
			<div class="vihaan-blog-lite-catdes">
				<header class="page-header">
					<?php
						the_archive_title( '<h2 class="page-title">', '</h2>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
					?>
				</header><!-- .page-header -->
			</div>
			<div class="post-loop-wrap clearfix" id="post-loop-wrap">
					<?php
					 $count = 1;	
					/* Start the Loop */
					while ( have_posts() ) : the_post(); ?>					
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
							<div class="vihaan-blog-lite-post-innr clearfix <?php if ( !has_post_thumbnail() ) { echo 'no-thumb-image'; } ?>"> 								
							<?php if( $count % 3 > 0 ) { ?> 
								<div class="vihaan-blog-lite-col-6 vihaan-blog-lite-columns">  
											<?php get_template_part( 'template-parts/content', 'media' ); ?>
								</div>	
								<div class="blogdesign-post-grid-content <?php if ( !has_post_thumbnail() ) { echo 'vihaan-blog-lite-col-12'; } else { echo 'vihaan-blog-lite-col-6';} ?>  vihaan-blog-lite-columns">				
										<header class="entry-header">											
												<?php  if (is_sticky()) : ?>
																<span class="sticky-label"><i class="fa fa-thumb-tack"></i><span class="sticky-label-text"><?php esc_html_e('Featured', 'vihaan-blog-lite'); ?></span></span>
												<?php endif; ?>
												<?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_cat_posted_on(); }   ?>
												<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>														
												<?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_posted_on(); }  ?>														
										</header><!-- .entry-header -->
										<div class="entry-content">
										<?php             
												$ismore = ! empty( $post->post_content ) ? strpos( $post->post_content, '<!--more-->' ) : '';
												if ( ! empty( $ismore ) ) {
														/* translators: %s: Name of current post */
														the_content( sprintf(
																		esc_html__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'vihaan-blog-lite' ),
																		get_the_title()
																) );
												} else {
														the_excerpt();				
												}
												wp_link_pages( array(
														'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vihaan-blog-lite' ),
														'after'  => '</div>',
												) );										
										 ?>
										 <?php vihaan_blog_lite_tags_posted_on(); ?>										 
											<footer class="entry-footer">
													<?php vihaan_blog_lite_entry_footer(); ?>
											</footer><!-- .entry-footer -->											
										</div><!-- .entry-content -->
								</div>  
							<?php } else { ?>	
								<div class="vihaan-blog-lite-col-12 vihaan-blog-lite-columns">									  
											 <div class="entry-media" >											 
											   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('large');  ?></a>
										   </div>								
											<div class="blogdesign-post-grid-content">				
													<header class="entry-header">											
															<?php  if (is_sticky()) : ?>
																			<span class="sticky-label"><i class="fa fa-thumb-tack"></i><span class="sticky-label-text"><?php esc_html_e('Featured', 'vihaan-blog-lite'); ?></span></span>
															<?php endif; ?>
															<?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_cat_posted_on(); }   ?>
															<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>														
															<?php if ( 'page' !== get_post_type() ) { vihaan_blog_lite_posted_on(); }  ?>														
													</header><!-- .entry-header -->
													<div class="entry-content">
													<?php             
															$ismore = ! empty( $post->post_content ) ? strpos( $post->post_content, '<!--more-->' ) : '';
															if ( ! empty( $ismore ) ) {
																	/* translators: %s: Name of current post */
																	the_content( sprintf(
																					esc_html__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'vihaan-blog-lite' ),
																					get_the_title()
																			) );
															} else {
																	the_excerpt();				
															}
															wp_link_pages( array(
																	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vihaan-blog-lite' ),
																	'after'  => '</div>',
															) );										
													 ?>
													 <?php vihaan_blog_lite_tags_posted_on(); ?>										 
														<footer class="entry-footer">
																<?php vihaan_blog_lite_entry_footer(); ?>
														</footer><!-- .entry-footer -->											
													</div><!-- .entry-content -->
											</div>  
								</div>	
							<?php } ?>			
							</div>
						</article><!-- #post-## -->						
					<?php $count ++ ;
					endwhile; ?>
                </div>
			<?php
			the_posts_pagination( array(
				'prev_text' => esc_html__( '&larr; Previous', 'vihaan-blog-lite' ),
				'next_text' => esc_html__( 'Next &rarr;', 'vihaan-blog-lite' ),
			) );

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>	
</div> <!-- Content-row -->
<?php
get_footer();