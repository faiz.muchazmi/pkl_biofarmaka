<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5UMV(/S(qoUhDgA^N#Bd~+_,:]dlM;;Rb>J`yBZbZJ!sQ)>D~hi|]7{N!&pfJ!.7');
define('SECURE_AUTH_KEY',  'qB027s|e~ZE^*6r&>hEpKpYt._0r?Zbg/1dF(64~kr-SE!+B5ox^d5J+3$c&JJWM');
define('LOGGED_IN_KEY',    'NOT~4mX<`XxVMQq-c^`r(Mb-m&}IW-ycA?v-oZ?H`p|L+{P;oO=(XvL2f#Kk8`Ey');
define('NONCE_KEY',        'fr8BJ0%|s1&@Z6Ko-Y*)k]^T!xdB;lgWo*`MQ~pCm`:#n$LuyDH3zLO<iSn#,}9Q');
define('AUTH_SALT',        '#r FnU.R.e#[DsTMSJB<<a[nvm!P)tR(Pfwjz]v!jK#`Yn[6XI,b>rTGARI<yN.k');
define('SECURE_AUTH_SALT', '(.14eD@[^jfcjk[{-7J/6-O,]_vg-cf-NQc5L07sjfPU$FTxZN-7SrJ8Oxbj3.y{');
define('LOGGED_IN_SALT',   'y|KC)&;45)K6)N1wF=(C<V8=y+wuSU3U*IG.7_TQtB6)/x-*zDb{N 04L>z^cl3!');
define('NONCE_SALT',       '`h>pBfOCt4suJ2f>#aE[Lfu%rIS8kY%76BHX5hzablZ1bOM6Zw@aL;U{l|A?IPr<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
