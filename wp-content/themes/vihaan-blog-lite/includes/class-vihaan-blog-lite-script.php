<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Vihaan_Blog_Lite_Script {
	
	function __construct() {

		// Action to add style in front end
		add_action( 'wp_enqueue_scripts', array($this, 'vihaan_blog_lite_front_styles'), 1 );

		// Action to add script in front end
		add_action( 'wp_enqueue_scripts', array($this, 'vihaan_blog_lite_front_scripts'), 1 );
    
	}
 

	/**
	 * Enqueue styles for front-end
	 * 
	 * @package Vihaan Blog Lite
	 * @since 1.0
	 */
	function vihaan_blog_lite_front_styles() {
		global $wp_styles;
		

		// Font Awesome CSS
		wp_register_style( 'font-awesome', VIHAAN_BLOG_LITE_URL . '/assets/css/font-awesome.min.css', array(), VIHAAN_BLOG_LITE_VERSION);
		wp_enqueue_style( 'font-awesome' );
		
		// Font Awesome CSS
		wp_register_style( 'owl-carousel', VIHAAN_BLOG_LITE_URL . '/assets/css/owl.carousel.min.css', array(), VIHAAN_BLOG_LITE_VERSION);
		wp_enqueue_style( 'owl-carousel' );			

		wp_enqueue_style( 'vihaan-blog-lite-fonts', vihaan_blog_lite_fonts_url(), array(), null );

		// Loads theme main stylesheet
		wp_enqueue_style( 'vihaan-blog-lite-style', get_stylesheet_uri(), array(), VIHAAN_BLOG_LITE_VERSION);

	}

	/**
	 * Enqueue scripts for front-end
	 * 
	 * @package Vihaan Blog Lite
	 * @since 1.0
	 */
	function vihaan_blog_lite_front_scripts() {	
	
		// Public Js
		wp_register_script( 'jquery-owl-carousel', VIHAAN_BLOG_LITE_URL . '/assets/js/owl.carousel.min.js', array('jquery'), VIHAAN_BLOG_LITE_VERSION, true);       
		wp_enqueue_script( 'jquery-owl-carousel' );
		
		// Public Js
		wp_register_script( 'vihaan-blog-lite-public-js', VIHAAN_BLOG_LITE_URL . '/assets/js/public.js', array('jquery'), VIHAAN_BLOG_LITE_VERSION, true);       
		wp_enqueue_script( 'vihaan-blog-lite-public-js' );
		
		/*
		 * Adds JavaScript to pages with the comment form to support
		 * sites with threaded comments (when in use).
		 */
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ){
			wp_enqueue_script( 'comment-reply' );
		}        
        
	}
}

$vihaan_blog_lite_script = new Vihaan_Blog_Lite_Script();