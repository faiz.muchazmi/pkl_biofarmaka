<?php
/**
 * Display an optional post thumbnail, video in according to post formats
 *
 * @package Vihaan Blog Lite
 * @since 1.0 
 */

if ( has_post_thumbnail() ) { ?>           
               <div class="entry-media" >
                  <?php if(is_single()) { ?> 
				   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('large');  ?></a>
				  <?php } else { ?>
				   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail();  ?></a>
				  <?php } ?>
				  
			   </div>
<?php }  ?>