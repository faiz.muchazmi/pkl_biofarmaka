<?php
/**
 *  Vihaan Blog Lite functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */

// Defining Some Variable
if( !defined( 'VIHAAN_BLOG_LITE_VERSION' ) ) {
	define('VIHAAN_BLOG_LITE_VERSION', '1.0.2'); // Theme Version
}
if( !defined( 'VIHAAN_BLOG_LITE_DIR' ) ) {
	define( 'VIHAAN_BLOG_LITE_DIR', get_template_directory() ); // Theme dir
}
if( !defined( 'VIHAAN_BLOG_LITE_URL' ) ) {
	define( 'VIHAAN_BLOG_LITE_URL', get_template_directory_uri() ); // Theme url
}


// Set up the content width value based on the theme's design and stylesheet.
if ( ! isset( $content_width ) )
	$content_width = 1170;
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vihaan_blog_lite_setup() {
	
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Vihaan Blog Lite, use a find and replace
	 * to change 'vihaan-blog-lite' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'vihaan-blog-lite', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
        
        // This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
        
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 640, 640, true );	

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(		
		'main-menu' 	=> esc_html__( 'Main Menu', 'vihaan-blog-lite' ),
		'footer-menu' 	=> esc_html__( 'Footer Menu', 'vihaan-blog-lite' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'vihaan_blog_lite_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add support for custom logo.
	add_theme_support( 'custom-logo' );

	// Post format.
	add_theme_support( 'post-formats', array('video', 'audio', 'quote', 'gallery'));
	
}
add_action( 'after_setup_theme', 'vihaan_blog_lite_setup' );

/**
 * Admin Welcome Notice
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_admin_welcom_notice() {
	global $pagenow;

	if ( is_admin() && isset( $_GET['activated'] ) && 'themes.php' === $pagenow ) {
		echo '<div class="updated notice notice-success is-dismissible"><p>'.sprintf( __( 'Thank you for choosing Vihaan Blog Lite Blog Theme. To get started, visit our <a href="%s">welcome page</a>.', 'vihaan-blog-lite' ), esc_url( admin_url( 'themes.php?page=vihaan-blog-lite' ) ) ).'</p></div>';
	}
}
add_action( 'admin_notices', 'vihaan_blog_lite_admin_welcom_notice' );

/**
	* Register Sidebars
	* 
	* @package Vihaan Blog Lite
	* @since 1.0
	*/
	function vihaan_blog_lite_register_sidebar() {

		// Main Sidebar Area
		register_sidebar( array(
			'name'          => __( 'Main Sidebar', 'vihaan-blog-lite' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Appears on posts and pages.', 'vihaan-blog-lite' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
		
		

		// Footer Sidebar Area
		register_sidebar( array(
			'name'          => __( 'Footer', 'vihaan-blog-lite' ),
			'id'            => 'footer',
			'description'   => __( 'Footer Widhet Area : Add widgets here.', 'vihaan-blog-lite' ),
			'before_widget' => '<section id="%1$s" class="widget vihaan-blog-lite-columns '. vihaan_blog_lite_footer_widgets_cls( 'footer' ) .' %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));

		// Footer Instgarm Widget Area
		register_sidebar( array(
			'name'          => __( 'Footer Instgarm Widget Area', 'vihaan-blog-lite' ),
			'id'            => 'vihaan-blog-lite-intsgram-feed',
			'description'   => __( 'Add widgets here.', 'vihaan-blog-lite' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
	}
	// Action to register sidebar
		
	add_action( 'widgets_init', 'vihaan_blog_lite_register_sidebar' );
	
if ( ! function_exists( 'vihaan_blog_lite_fonts_url' ) ) {
/**
 * Register Google fonts for Vihaan Blog Lite.
 * Create your own vihaan_blog_lite_fonts_url() function to override in a child theme.
 * @return string Google fonts URL for the theme.
 */
function vihaan_blog_lite_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Poppins, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Poppins font: on or off', 'vihaan-blog-lite' ) ) {
		$fonts[] = 'Poppins:400,500,700';
	}
	/* translators: If there are characters in your language that are not supported by Roboto, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Roboto font: on or off', 'vihaan-blog-lite' ) ) {
		$fonts[] = 'Roboto:400,500';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
}	
/**
/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 *
 * @package Vihaan Blog Lite
 * @since 1.0
 */
function vihaan_blog_lite_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'vihaan_blog_lite_pingback_header', 5 );

// Common Functions File
require_once VIHAAN_BLOG_LITE_DIR . '/includes/vihaan-blog-lite-functions.php';

// Custom template tags for this theme
require_once VIHAAN_BLOG_LITE_DIR . '/includes/template-tags.php';

// Theme Customizer Settings
require_once VIHAAN_BLOG_LITE_DIR . '/includes/customizer.php';

// Script Class
require_once( VIHAAN_BLOG_LITE_DIR . '/includes/class-vihaan-blog-lite-script.php' );

// Theme Dynemic CSS
require_once( VIHAAN_BLOG_LITE_DIR . '/includes/vihaan-blog-lite-theme-css.php' );

/**
 * Load tab dashboard
 */
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
    require get_template_directory() . '/includes/dashboard/vihaan-blog-lite-how-it-work.php';
    
}

/************Category image Module Module End******************/

// Plugin recomendation class
require_once( VIHAAN_BLOG_LITE_DIR . '/includes/plugins/class-wpcrt-recommendation.php' );